package bai1buoi9;

import java.util.Scanner;

public class VietnameseCustomer extends InvoiceList {
    private Scanner sc = new Scanner(System.in);
    private String typeofCustomer;
    private double comsumptionNorms;

    public VietnameseCustomer(String customerID, String name, String dateofInvoice, double amuont, double price, String typeofCustomer, double comsumptionNorms) {
        super(customerID, name, dateofInvoice, amuont, price);
        this.typeofCustomer = typeofCustomer;
        this.comsumptionNorms = comsumptionNorms;
    }

    public String getTypeofCustomer() {
        return typeofCustomer;
    }

    public void setTypeofCustomer(String typeofCustomer) {
        this.typeofCustomer = typeofCustomer;
    }

    public double getComsumptionNorms() {
        return comsumptionNorms;
    }

    public void setComsumptionNorms(double comsumptionNorms) {
        this.comsumptionNorms = comsumptionNorms;
    }

    public void importInfor() {
        super.importInfor();
        System.out.println("Nhập đối tượng khách hàng: ");
        this.typeofCustomer = sc.nextLine();
        System.out.println("Nhập định mức tiêu thụ: ");
        this.comsumptionNorms = sc.nextDouble();
    }

    public double totalPrice () {

        if(this.getAmuont()<=this.comsumptionNorms)
        {
            return this.getAmuont()*this.getPrice();
        }
        else
        {
            return this.getAmuont()*this.getPrice()
                    +(this.comsumptionNorms-this.getAmuont())*this.getPrice()*2.5;
        }
    }

    public void exportInfor() {
        super.exportInfor();
        System.out.println("Đối tượng khách hàng: "+this.typeofCustomer);
        System.out.println("Định mức tiêu thụ: "+this.comsumptionNorms);
        System.out.println("Tổng hóa đơn: "+this.totalPrice());
        System.out.println(" ");
    }


}
