package bai1buoi9;

import java.util.Scanner;

public class InvoiceList {
    private Scanner sc = new Scanner(System.in);
    private String customerID;
    private String name;
    private String dateofInvoice;
    private double amuont;
    private double price;

    public InvoiceList(String customerID, String name, String dateofInvoice, double amuont, double price) {
        this.customerID = customerID;
        this.name = name;
        this.dateofInvoice = dateofInvoice;
        this.amuont = amuont;
        this.price = price;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateofInvoice() {
        return dateofInvoice;
    }

    public void setDateofInvoice(String dateofInvoice) {
        this.dateofInvoice = dateofInvoice;
    }

    public double getAmuont() {
        return amuont;
    }

    public void setAmuont(double amuont) {
        this.amuont = amuont;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void importInfor() {
        System.out.println("Nhập thông tin khách hàng: ");
        System.out.println("Nhập mã khách hàng: ");
        this.customerID = sc.nextLine();
        System.out.println("Nhập họ và tên khách hàng: ");
        this.name = sc.nextLine();
        System.out.println("Nhập ngày xuất hóa đơn: ");
        this.dateofInvoice = sc.nextLine();
        System.out.println("Nhập số lượng tiêu thụ (kW): ");
        this.amuont = sc.nextDouble();
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập đơn giá (VNĐ): ");
        this.price = sc.nextDouble();
    }

    public double totalPrice () {
        return this.amuont * this.price;

    }

    public void exportInfor() {
        System.out.println("Xuất thông tin khách hàng: ");
        System.out.println("Mã khách hàng: "+this.customerID);
        System.out.println("Họ và tên khách hàng: "+this.name);
        System.out.println("Ngày xuất hóa đơn: "+this.dateofInvoice);
        System.out.println("Số lượng tiêu thụ (kW): "+this.amuont);
        System.out.println("Đơn giá (VNĐ): "+this.price);
        System.out.println("Tổng tiền: "+totalPrice());
    }




}
