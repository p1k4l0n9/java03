package bai1buoi9;

public class Main {
    public static void main(String[] args) {
        VietnameseCustomer vietnameseCustomer1 = new VietnameseCustomer("","","",0,0,"",0);
        VietnameseCustomer vietnameseCustomer2 = new VietnameseCustomer("","","",0,0,"",0);
        ForeignCustomer foreignCustomer1 = new ForeignCustomer("","","",0,0,"");
        ForeignCustomer foreignCustomer2 = new ForeignCustomer("","","",0,0,"");

        vietnameseCustomer1.importInfor();
        vietnameseCustomer2.importInfor();
        foreignCustomer1.importInfor();
        foreignCustomer2.importInfor();

        vietnameseCustomer1.exportInfor();
        vietnameseCustomer1.exportInfor();
        foreignCustomer1.exportInfor();
        foreignCustomer2.exportInfor();

        double averagePrice;
        averagePrice = (foreignCustomer1.totalPrice()+foreignCustomer2.totalPrice())/2;
        System.out.println("Trung bình thành tiền hóa đơn của khách nước ngoài là: "+averagePrice);

        System.out.println("Danh sách khách hàng nhận hóa đơn trong tháng 9 năm 2019: ");
        if ((vietnameseCustomer1.getDateofInvoice()).contains("09/2019"))
            vietnameseCustomer1.exportInfor();
        if ((vietnameseCustomer2.getDateofInvoice()).contains("09/2019"))
            vietnameseCustomer2.exportInfor();
        if ((foreignCustomer1.getDateofInvoice()).contains("09/2019"))
            foreignCustomer1.exportInfor();
        if ((foreignCustomer2.getDateofInvoice()).contains("09/2019"))
            foreignCustomer2.exportInfor();

    }
}
