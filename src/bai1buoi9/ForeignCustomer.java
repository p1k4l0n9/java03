package bai1buoi9;

import java.util.Scanner;

public class ForeignCustomer extends InvoiceList {
    private Scanner sc = new Scanner(System.in);
    private String nationality;

    public ForeignCustomer(String customerID, String name, String dateofInvoice, double amuont, double price, String nationality) {
        super(customerID, name, dateofInvoice, amuont, price);
        this.nationality = nationality;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void importInfor() {
        super.importInfor();
        System.out.println("Nhập quốc tịch của khách hàng: ");
        this.nationality = sc.nextLine();
    }

    public double totalPrice() {
        return this.getAmuont()*this.getPrice();
    }

    public void exportInfor() {
        super.exportInfor();
        System.out.println("Quốc tịch khách hàng: "+this.nationality);
        System.out.println("Tổng hóa đơn: "+this.totalPrice());
        System.out.println("");
    }




}
